#  Data transmission over a secure channel with STM32F429ZI




This project is my 3rd semester minicourse in the Physics Department of the MSU. 

## Mission statement
Development of a device for two-way communication over a secure channel.The device 
must receive messages from a PC, encrypt them and send them to the other device.
On receipt of an encrypted message, the device must decrypt it and send it to the PC
## Features

-  A symmetric block cipher algorithm, adopted as the encryption standard by the US 
   government following an AES competition
- The UART interface leaves a wide range of solutions for data transmission
- Your conversation partner must know the encryption key
- Any terminal as a workspace




## Components required:



- Two STM32F429zi-Discovery series boards
- Two USB to UART CP210 adapters, connection cables
- Two PCs with IAR Embdded Workbench (version no lower than 8.30.1) and STM32CubeMX (download last version and everything will be OK)
- The only library used that is not from the standard list: http://www.how.net.ua/2015/05/kak-ispolzovat-aes-s-stm32/

Connecting cables are very restrictive to the working space. In another project I used the SV610, a very simple and easy to use transmit and receive sensor. The other libraries are generated automatically when the project is built




## Assembly guide

It is assumed that the user is familiar with the IAR environment and already has it installed. 

1. Transfer the project folder to the PC to be used.
2.	Create a workspace or, if one has already been created, add the .ewp project to the workspace
3.	Download [PuTTy](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html) (I used it) or any other terminal. Set the baud rate to 115200. In Task Manager, see the port to which the USB to UART adapter is connected. Specify it in the terminal. Download the appropriate drivers for the adapter. For CP210 [here](https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers)
4.	Assemble the channel from the specified components. Pay attention to the specification 
of the UART connection. Without grounding, junk data streams are possible.
5.	Repeat points 1-3 on another PC

>Note: the project may not work without CubeMX installed as
initially projects were built there on two PCs. However, the program
can save your time in case of changing any basic functions (speed, UART change).

## Other

In addition to the code folder, I've uploaded a presentation of the work (in Russian). Files "UART_connecting.c" and "Ready_enc_dec.c" were used for testing and are not used in the finished project. I'm attaching them just for reference  I'd be happy if it was useful to someone.All the questions can be send by telegram:: https://t.me/Kostyll80
