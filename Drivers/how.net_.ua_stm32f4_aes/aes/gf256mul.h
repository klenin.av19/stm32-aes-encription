#ifndef G256_H_
#define G256_H_

#include "stm32f4xx.h"

uint8_t gf256mul(uint8_t a, uint8_t b, uint8_t reducer);

#endif
