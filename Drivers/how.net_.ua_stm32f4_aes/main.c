#include "stm32f4xx_conf.h"

#include "aes.h"

uint8_t AES_key[16];
aes128_ctx_t AES_ctx;

uint8_t data[16];

uint8_t getRND(void) {
	uint32_t rnd = 0;
	while(RNG_GetFlagStatus(RNG_FLAG_DRDY) == RESET);
	rnd = RNG_GetRandomNumber();

	return (uint8_t)rnd;
}

int main(void)
{
	uint32_t i = 0;

	SystemInit();

	// �������� ������������ �� ������ ���������� ���������� �����
	RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE);
	// �������� ���������
	RNG_Cmd(ENABLE);

	// �������� ��������� ������� ���� � �������� ������ ������
	for (;i < 16; i++) {
		AES_key[i] = getRND();
		data[i] = i;
	}

	// ������������� AES
	aes128_init(AES_key, &AES_ctx);

	// ������� ������ 128 ���
	aes128_enc(data, &AES_ctx);

	// ��������� ������ 128 ���
	aes128_dec(data, &AES_ctx);


    while(1);
}
