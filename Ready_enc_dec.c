/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include "aes.h"

//#include "usart.h"
//#include "gpio.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART3_UART_Init(void);
/* USER CODE BEGIN PFP */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart);


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t AES_key[10];
uint8_t str[16];
uint8_t str2[3];
uint8_t str3[3];
uint8_t mode[3];
uint8_t dataReceived=0; 
uint8_t dataTransmitted=1;
const char message[30] = "Input your key:\r\n";
int i,j,k= 0;
aes128_ctx_t AES_ctx;
uint8_t buf[16];


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  const char greeting[] = "My greeting!\r\n"; 
  const char about_mode[] = "You can transive o receive data. For choosing input '1' (receive) or '2' (transive):\r\n";
  const char TX_mode[] = "You will transive data.\r\n";
  const char RX_mode[] = "You will receive data.\r\n";
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_UART_Transmit(&huart1,(uint8_t*) greeting,strlen(greeting) , 3000); // Input your message
  HAL_UART_Transmit(&huart1,(uint8_t*) message,strlen(message) , 3000);
  
  //Read user's key
  while (*str3 != '\r' )
  {
    if( HAL_UART_Receive(&huart1, str3, 1, 3) == HAL_OK ) 
    {
      AES_key[i]=*str3;
      i++;
      HAL_UART_Transmit(&huart1, str3, 1, 3);
    }
  }
  HAL_UART_Transmit(&huart1,"\r\n",4 , 30);
  HAL_UART_Transmit(&huart1,(uint8_t*) about_mode,strlen(about_mode) , 3000);
  
  while (*str2!= '\r' )
  {
    if( HAL_UART_Receive(&huart1, str2, 1, 3) == HAL_OK ) 
    {
      mode[j]=*str2;
      j++;
      HAL_UART_Transmit(&huart1, str2, 1, 3);   
    }
  }
  HAL_UART_Transmit(&huart1,"\r\n",4 , 3000);
  
  // for testing AES
/*  while (*str3 != '\r')
  {
    if( HAL_UART_Receive(&huart1, str3, 1, 3) == HAL_OK ) 
    {
      buf[k]=*str3;
      k++;
      HAL_UART_Transmit(&huart1, str3, 1, 3);   
    }
  }
  HAL_UART_Transmit(&huart1,"\r\n",4 , 3000);
  
 
 
  aes128_enc(buf, &AES_ctx);
  
  aes128_dec(buf, &AES_ctx);*/
 
 
  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  aes128_init(AES_key, &AES_ctx);
  if (mode[0] == '2')
  { 
    HAL_UART_Transmit(&huart1,(uint8_t*) TX_mode,strlen(TX_mode) , 3000);
    HAL_UART_Receive_IT (&huart1, str, 1);
  }
  
  if (mode[0] == '1')
  {
    HAL_UART_Transmit(&huart1,(uint8_t*) RX_mode,strlen(RX_mode) , 3000);
    HAL_UART_Receive_IT (&huart3, str, 16);
  }   
   
  while (1)
  {
    /* USER CODE END WHILE */
   
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */



// for UART1
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {

   if(mode[0] == '2')
  {
    if(huart == &huart1) 
     {
      dataReceived=1;
      
      if( dataTransmitted != 0 ) 
      {
        aes128_enc(str, &AES_ctx);     
        //HAL_UART_Transmit_IT(&huart1, str, 16);  
        HAL_UART_Transmit_IT(&huart3, str, 16);
        dataReceived=0;
        dataTransmitted=0;  
      }
    HAL_UART_Receive_IT (&huart1, str, 1);
    }
  }
  
  if(mode[0] == '1')
  {
    if(huart == &huart3)
    {
      dataReceived=1;
      
    if( dataTransmitted != 0 ) 
    {
      aes128_dec(str, &AES_ctx);
      HAL_UART_Transmit_IT(&huart1, str, 1);
      dataReceived=0;
      dataTransmitted=0;  
    }
    HAL_UART_Receive_IT (&huart3, str, 16);
    }
  }
  }


void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) { 
    dataTransmitted=1;
  
  }
 
 
 




/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
